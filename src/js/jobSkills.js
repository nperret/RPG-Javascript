//Warrior

let gaiaClash = new Skill(0,0,10,0,"Gaïa Clash");
let fireSlash=new Skill(0,10,0,0,"Fire Slash");
let neptuneHit=new Skill(10,0,0,0,"Neptune hit");

//thief 
let pocketRob = new Skill(4,4,2,0,"Rob the pocket");
let backHit=new Skill(2,4,4,0,"Hit the back");
let darkSlash=new Skill(0,0,0,10,"Slice in the night");

//Wizzard
let vanishingBloom = new Skill(5,3,2,0,"Vanishing Bloom");
let dragoonBreath=new Skill(0,5,2,3,"Breath of dragoon");
let poseidonValkyries=new Skill(5,0,0,5,"Valkyre if Poseidon");

//paladin
let hammerBlossom = new Skill(0,0,2,0,"Hammer of Blossom");
let staffLight=new Skill(0,4,4,0,"Hit by the light");
let darkSlash=new Skill(0,0,0,10,"Slice in the night");



/**
 * 
 * @param {string} from the source of the damage
 * @param {string} to the char wounded
 * @param {number} dmg the number of damage
 */

export function addLogCombat(from, to, dmg) {
  let logCombat = document.createElement('p');
  logCombat.textContent = `${to} take ${dmg} damages from ${from}`;

  let logScroll = document.querySelector('#scroll');
  logScroll.prepend(logCombat);

  return dmg
}

/**
 * 
 * @param {number} skill base attribute from the profession's damage 
 */
export function computeBonus(skill) {
  if (character.job === "warrior") {

    skill = character.strength
    return skill / 10;
  }

  if (character.job === "wizard") {

    skill = character.magic
    return skill / 10;
  }

  if (character.job === "thief") {

    skill = character.strength + 5
    return skill / 10;
  }

  if (character.job === "paladin") {

    skill = (character.strength + character.magic) / 2
    return skill / 10;
  }

}



export function iaJob(job) {

  let iaJob1 = "";
  let iaJob2 = "";
  let iaJob3 = "";


  switch (character.job) {
    case "warrior":

      iaJob1 = "thief";
      iaJob2 = "paladin";
      iaJob3 = "wizard";
      break;

      case "thief":

      iaJob1 = "warrior";
      iaJob2 = "paladin";
      iaJob3 = "wizard";
      break;

      case "paladin":

      iaJob1 = "thief";
      iaJob2 = "warrior";
      iaJob3 = "wizard";
      break;

      case "wizard":

      iaJob1 = "thief";
      iaJob2 = "paladin";
      iaJob3 = "warrior";
      break;

    default:
      break;
  }

}


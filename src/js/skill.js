export class Skill {
  constructor(water=0,fire=0,earth=0,shadow=0,name=''){
    this.waterDmg=water,
    this.fireDmg=fire,
    this.earthDmg=earth,
    this.shadowDmg=shadow,
    this.skillName=name
  }

  dealWaterDmg(bonus){
    return this.waterDmg*bonus
  }

  dealFireDmg(bonus){
    return this.fireDmg*bonus
  }

  dealEarthDmg(bonus){
    return this.earthDmg*bonus
  }

  dealShadowDmg(bonus){
    return this.shadowDmg*bonus
  }

  dealAllDmg(bonus){
    return (this.waterDmg+this.fireDmg+this.earthDmg+this.shadowDmg)*bonus
  }
}
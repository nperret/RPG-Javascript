const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports={
  mode:'development',
  entry:'./src/js/index.js',
  output:{
    filename:'./bundle.js'
  },

  module: {
    rules: [{
      test: /\.s?css$/,
      use: [

        MiniCssExtractPlugin.loader,
        "style-loader", // creates style nodes from JS strings
        "css-loader", // translates CSS into CommonJS
        "sass-loader" // compiles Sass to CSS
      ]
    }]
  }

}
